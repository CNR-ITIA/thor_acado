
#include <acado_toolkit.hpp>
#include <acado_gnuplot.hpp>
#include <ros/ros.h>
//#include <thor_acado/thor_acado.h>
#include <rosdyn_core/primitives.h>
#include <moveit_msgs/ExecuteTrajectoryAction.h>
#include <moveit_planning_helper/ExecuteTrajectoryFromParamAction.h>
#include <moveit_planning_helper/manage_trajectories.h>
#include <actionlib/client/simple_action_client.h>

rosdyn::ChainPtr chain;
double Ttot=5.0;
Eigen::VectorXd qstart;
Eigen::VectorXd amplitude;

void funz( double *x, double *f, void *user_data )
{

  int m_ndof=chain->getActiveJointsNumber();
  Eigen::Map<Eigen::VectorXd> q(x,m_ndof);
  Eigen::Map<Eigen::VectorXd> dq(x+m_ndof,m_ndof);
  Eigen::Map<Eigen::VectorXd> tau(x+2*m_ndof,m_ndof);
  Eigen::Map<Eigen::VectorXd> t(x+3*m_ndof,1);

  ROS_INFO_THROTTLE(60,"I am alive");
  Eigen::VectorXd ddq=chain->getJointInertia(q).inverse()*(tau-chain->getJointTorqueNonLinearPart(q,dq));
  Eigen::Map<Eigen::VectorXd> dx(f,m_ndof);
  dx<<ddq;
}

void trj( double *x, double *f, void *user_data )
{
  int m_ndof=chain->getActiveJointsNumber();
  Eigen::Map<Eigen::VectorXd> th(x,1);
  double theta=th(0);
  if (theta>Ttot)
    theta=Ttot;
  double ldm = 6*pow(theta/Ttot,5) - 15*pow(theta/Ttot,4) + 10*pow(theta/Ttot,3);

  Eigen::VectorXd qdes(m_ndof);
  for (int idx=0;idx<m_ndof;idx++)
    qdes(idx)=qstart(idx)+amplitude(idx)*sin(2*M_PI*ldm);
  Eigen::Map<Eigen::VectorXd> out(f,m_ndof);
  out<<qdes;
}

int main(int argc, char **argv){
  ros::init(argc, argv, "thor_acado_torque");
  ros::NodeHandle nh;
  actionlib::SimpleActionClient<moveit_planning_helper::ExecuteTrajectoryFromParamAction> ac("/execute_trajectories_from_param");

  ROS_INFO("get params");
  std::string base_frame = "ur10_base_link";
  std::string tool_frame = "ur10_forearm_link";
  Ttot=5.0;

  std::vector<double> predictive_horizons;
  std::vector<double> trj_time;
  std::vector<int> np;
  unsigned int number_instants=10;


  if (!nh.getParam("predictive_horizon",predictive_horizons))
  {
    ROS_ERROR("predictive_horizon not found");
    return 0;
  }
  if (!nh.getParam("number_instants",np))
  {
    ROS_ERROR("number_instants not found");
    return 0;
  }

  if (!nh.getParam("total_time",trj_time))
  {
    ROS_ERROR("total_time not found");
    return 0;
  }
  if (!nh.getParam("base_frame",base_frame))
  {
  }
  if (!nh.getParam("tool_frame",tool_frame))
  {
  }

  ROS_INFO("creating rosdyn chain");
  urdf::Model model;
  model.initParam("/robot_description");
  Eigen::Vector3d grav;
  grav << 0, 0, -9.806;
  chain = rosdyn::createChain(model,base_frame,tool_frame,grav);
  if (!chain)
  {
    ROS_ERROR("chain creation fail");
    return 0;
  }
  unsigned int ndof=chain->getActiveJointsNumber();
  ROS_INFO("chain created!");

  qstart.resize(6);
  qstart << 0.0, -2.0, 0.0, 0.0, 0.0, 0.5;
  qstart.conservativeResize(ndof);
  amplitude.resize(ndof);
  amplitude.setConstant(0.5);
  std::vector<double> temp;
  if (nh.getParam("qstart",temp))
  {
    for (unsigned int idx=0;idx<ndof;idx++)
      qstart(idx)=temp.at(idx);
  }

  if (nh.getParam("amplitude",temp))
  {
    for (unsigned int idx=0;idx<ndof;idx++)
      amplitude(idx)=temp.at(idx);
  }

  ROS_INFO("initiazing acado");
  USING_NAMESPACE_ACADO
      ACADO::CFunction simple_integrator( ndof, funz);
  ACADO::CFunction trajectory_gen( ndof, trj);
  DifferentialState q("q",ndof,1);
  DifferentialState dq("dq",ndof,1);
  DifferentialState theta;
  DifferentialState dtheta;
  Control tau("tau",ndof,1);
  Control theta_c;
  ACADO::TIME t;
  ACADO::IntermediateState x(3*ndof+1);
  for (unsigned int idx=0;idx<ndof;idx++)
  {
    x(idx)=q(idx);
    x(ndof+idx)=dq(idx);
    x(2*ndof+idx)=tau(idx);
  }
  x(3*ndof)=t;

  ACADO::IntermediateState ddq(ndof);
  ddq=simple_integrator(x);

  DifferentialEquation f;
  f << dot(q) == dq;
  f << dot(dq) == ddq;
  f << dot(theta) == dtheta;
  f << dot(dtheta) == theta_c;

  ROS_INFO("initiazing opc");
  Function h;
  h << q - trajectory_gen(theta);
  h << tau;
  h << dtheta;
  h << theta_c;
  DMatrix S(2*ndof+2,
            2*ndof+2);
  DVector r(2*ndof+2);
  S.setIdentity();
  for (unsigned int idx=0;idx<ndof;idx++) // position weights
    S(idx,idx)=1.0e8;
  for (unsigned int idx=0;idx<ndof;idx++) // torque weights
    S(ndof+idx,ndof+idx)=0.5;
  S(2*ndof,2*ndof)=1e5;      // dtheta weight
  S(2*ndof+1,2*ndof+1)=1e-7; // theta_c weight
  r.setAll( 0.0 );
  r(2*ndof)=1; // dtheta reference (sref=1)

  for (unsigned int itime=0;itime<trj_time.size();itime++)
  {


    Ttot=trj_time.at(itime);
    for (unsigned int ipred=0;ipred<predictive_horizons.size();ipred++)
    {
      double predictive_horizon=predictive_horizons.at(ipred);
      number_instants=np.at(ipred);

      const double t_start = 0.0;
      const double t_end   = predictive_horizon;
      const unsigned int np=number_instants;
      OCP ocp( t_start, t_end, np );
      ocp.minimizeLSQ( S, h, r );

      DVector torque_lim(6);
      torque_lim << 200, 200, 100, 50, 50, 50;
      DVector vel_lim(6);
      vel_lim << 2, 2, 3, 3, 3, 3;
      DVector acc_lim(6);
      acc_lim << 5, 5, 10, 10, 10, 10;

      ocp.subjectTo( f );
      for (unsigned int idx=0;idx<ndof;idx++)
      {
        ocp.subjectTo( -torque_lim(idx) <= tau(idx) <= torque_lim(idx) );
        ocp.subjectTo( -vel_lim(idx) <= dq(idx) <= vel_lim(idx) );
        ocp.subjectTo( -vel_lim(idx) <= dq(idx) <= vel_lim(idx) );
        ocp.subjectTo( -acc_lim(idx) <= ddq(idx) <= acc_lim(idx) );
      }
      ocp.subjectTo( 0.0 <= dtheta <= 10.0 );
      ocp.subjectTo( -1e6 <= theta_c <= 1e6 );

      ROS_INFO("creating controller");
      double sampling_time=1e-3;
      RealTimeAlgorithm algorithm(ocp,sampling_time);
      GnuplotWindow window1;
      window1.addSubplot(   q(0),"Position [rad]" );
      window1.addSubplot(  dq(0),"Velocity [rad/s]" );
      window1.addSubplot( tau(0),"Damping Force [Nm]" );
      algorithm << window1;

      //  algorithm.set( HESSIAN_APPROXIMATION, GAUSS_NEWTON_WITH_BLOCK_BFGS );
      int iter_sqp=1;
      if (!nh.getParam("iter_sqp",iter_sqp))
      {
        ROS_INFO("iter_sqp = %f",iter_sqp);
      }

      algorithm.set( KKT_TOLERANCE, 1e-6 );
      algorithm.set (MAX_NUM_ITERATIONS,iter_sqp);
      algorithm.set( INTEGRATOR_TYPE, INT_IRK_GL2 );
      algorithm.set(MAX_NUM_INTEGRATOR_STEPS,100000);
      algorithm.set(PRINTLEVEL,NONE);
      //  algorithm.set (MAX_NUM_QP_ITERATIONS,1);

      ROS_INFO("create integrator for simulation");
      IntegrationAlgorithm intAlg;
      Grid timeHorizon( 0,sampling_time );
      intAlg.addStage( f, timeHorizon, INT_RK45 );
      intAlg.set( INTEGRATOR_PRINTLEVEL, NONE );
      intAlg.set( INTEGRATOR_TOLERANCE, 1.0e-6 );
      intAlg.set(MAX_NUM_INTEGRATOR_STEPS,100000);

      ROS_INFO("define trajectory and initialize states");
      DVector x0(2*ndof+2);
      DVector measure(2*ndof+2);
      measure.setZero();
      x0.setZero();
      for (unsigned int idx=0;idx<ndof;idx++)
      {
        x0(idx)=qstart(idx);
        measure(idx)=qstart(idx);
      }

      ROS_INFO("solve first step");
      ros::Time t0=ros::Time::now();
      algorithm.init(0,x0);
      ROS_INFO("init solved in %f [ms]",(ros::Time::now()-t0).toSec()*1e3);
      t0=ros::Time::now();
      algorithm.step(0,measure,emptyConstVector);
      ROS_INFO("first step solved in %f [ms]",(ros::Time::now()-t0).toSec()*1e3);

      DVector u(ndof+1);
      intAlg.integrate( timeHorizon, x0,emptyConstVector,emptyConstVector,u );
      DVector differentialStates;
      intAlg.getX( differentialStates );

      ROS_INFO("solving trajectory");
      double current_time=0.0;
      double max_computing_time=0.0;
      double mean_computing_time=0.0;
      double mean_scaling=1.0;
      std::string test_name = "nmpc_torque_Ttot_" + std::to_string(int(Ttot)) + "_Np" + std::to_string(int(number_instants)) + "_T" + std::to_string(int(predictive_horizon*1000));
      moveit_msgs::ExecuteTrajectoryGoal goal;
      goal.trajectory.joint_trajectory.joint_names.resize(ndof);
      goal.trajectory.joint_trajectory.joint_names=chain->getMoveableJointNames();

      ros::Duration time=ros::Duration(0.0);
      std::string status="success";
      for (int idx=0;idx<int((Ttot+1)/sampling_time);idx++)
      {
        //    if (idx>100)
        //      break;
        if (!ros::ok())
          return 0;
        current_time+=sampling_time;
        intAlg.getX(measure);
        t0=ros::Time::now();
        algorithm.step(0,measure,emptyConstVector);
        double computing_time=(ros::Time::now()-t0).toSec()*1e3;
        if (max_computing_time<computing_time)
          max_computing_time=computing_time;
        mean_computing_time=(mean_computing_time*((double)idx)+computing_time)/(double)(idx+1.);
        mean_scaling=(mean_scaling*idx+measure(2*ndof+1))/(idx+1);
        algorithm.getU(u);
        if (!intAlg.integrate( timeHorizon, measure, emptyConstVector, emptyConstVector, u ))
        {
          ROS_ERROR("trajectory %s integration error",test_name.c_str());
          status="integration error";
          break;
        }

        /* SEND TO CONTROLLER */
        Eigen::VectorXd q_ref=measure.head(ndof);
        Eigen::VectorXd dq_ref=measure.segment(ndof,ndof);
        Eigen::VectorXd tau_ref=u.head(ndof);

        trajectory_msgs::JointTrajectoryPoint pnt;
        pnt.positions.resize(ndof);
        pnt.velocities.resize(ndof);
        pnt.effort.resize(ndof);

        /* log */
        for (unsigned int idof=0;idof<ndof;idof++)
        {
          pnt.positions.at(idof)=q_ref(idof);
          pnt.velocities.at(idof)=dq_ref(idof);
          pnt.effort.at(idof)=tau_ref(idof);
        }
        pnt.time_from_start=time;
        time+=ros::Duration(sampling_time);
        goal.trajectory.joint_trajectory.points.push_back(pnt);

        ROS_INFO("test=%s, perc=%f%%",test_name.c_str(),current_time/(Ttot+1)*100);
	if (idx==10 || idx==20)
	{
        	ROS_ERROR("mean computing time = %f [ms]",mean_computing_time);
        	ROS_ERROR("max computing time = %f [ms]",max_computing_time);
	}
      }
      ROS_ERROR("mean scaling = %f",mean_scaling);
      ROS_ERROR("max computing time = %f [ms]",max_computing_time);
      ROS_ERROR("mean computing time = %f [ms]",mean_computing_time);


      nh.setParam("/acado_results/"+test_name+"/status",status);

      nh.setParam("acado_results/"+test_name+"/mean_scaling",mean_scaling);
      nh.setParam("acado_results/"+test_name+"/max_computing_time",max_computing_time);
      nh.setParam("acado_results/"+test_name+"/mean_computing_time",mean_computing_time);

      trajectory_processing::setTrajectoryToParam(nh,"acado_results/"+test_name+"/trj",goal.trajectory.joint_trajectory);

      system("rosparam dump nmpc_torque.yaml acado_results/");
    }
  }
  //  moveit_planning_helper::ExecuteTrajectoryFromParamGoal exec_goal;
  //  exec_goal.group_name="ur10";
  //  exec_goal.simulation=false;
  //  exec_goal.trajectory_names.push_back("nmpc");
  //  if (!ac.waitForServer(ros::Duration(10)))
  //  {
  //    ROS_ERROR("no server found");
  //    return 0;
  //  }
  //  ac.sendGoalAndWait(exec_goal);

  return 0;







//  ROS_INFO("starting acado simulation");
//  StaticReferenceTrajectory zeroReference;
//  Controller controller( algorithm,zeroReference );
//  OutputFcn identity;
//  DynamicSystem dynamicSystem( f,identity );
//  Process process( dynamicSystem, INT_RK45 );
//  SimulationEnvironment sim( 0.0,Ttot,process,controller );
//  sim.init( x0 );
//  t0=ros::Time::now();
//  sim.run( );
//  double average_iter_time = (ros::Time::now()-t0).toSec()*1e3 / (Ttot/sampling_time);
//  ROS_INFO("average computing time = %f [ms]",average_iter_time);

//  ROS_INFO("plot results");
//  VariablesGrid diffStates;
//  sim.getProcessDifferentialStates( diffStates );
//  VariablesGrid feedbackControl;
//  sim.getFeedbackControl( feedbackControl );
//  GnuplotWindow window;
//  window.addSubplot( diffStates(1),   "q1 [m]" );
//  window.addSubplot( diffStates(ndof+1),   "dq1 [m]" );
//  window.addSubplot( diffStates(2*ndof),   "theta" );
//  window.addSubplot( diffStates(2*ndof+1),   "dtheta" );
//  window.addSubplot( feedbackControl(1), "torque" );
//  window.addSubplot( feedbackControl(ndof), "ddtheta" );
//  window.plot( );

//  return 0;
}



