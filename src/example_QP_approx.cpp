#include <thor_math/thor_math.h>
#include <ros/ros.h>
#include <sensor_msgs/JointState.h>
#include <std_msgs/Float64.h>
#include <fstream>
#include <moveit_msgs/ExecuteTrajectoryAction.h>
#include <moveit_planning_helper/ExecuteTrajectoryFromParamAction.h>
#include <moveit_planning_helper/manage_trajectories.h>
#include <actionlib/client/simple_action_client.h>

double Ttot=5.0;
Eigen::VectorXd qstart;
Eigen::VectorXd amplitude;

double period=0.5;

void trj( double theta, const unsigned int ndof, Eigen::VectorXd& qdes, Eigen::VectorXd& dqdes )
{
  if (theta>Ttot)
      theta=Ttot;
  double ldm = 6*pow(theta,5)/pow(Ttot,5) - 15*pow(theta,4)/pow(Ttot,4) + 10*pow(theta,3)/pow(Ttot,3);
  double ldm_d = 5*6*pow(theta,4)/pow(Ttot,5) - 4*15*pow(theta,3)/pow(Ttot,4) + 3*10*pow(theta,2)/pow(Ttot,3);

  qdes.resize(ndof);
  dqdes.resize(ndof);
  for (int idx=0;idx<ndof;idx++)
  {
    qdes(idx)=qstart(idx)+amplitude(idx)*sin(2*M_PI*ldm);
    dqdes(idx)=amplitude(idx)*2*M_PI*cos(2*M_PI*ldm)*ldm_d;
  }
}

int main(int argc, char **argv){
  ros::init(argc, argv, "example_QP_approx");
  ros::NodeHandle nh;
  actionlib::SimpleActionClient<moveit_planning_helper::ExecuteTrajectoryFromParamAction> ac("/execute_trajectories_from_param");

  ROS_INFO("get params");
  std::string base_frame = "ur10_base_link";
  std::string tool_frame = "ur10_forearm_link";
  bool INPUT_BLK=false;
  Ttot=5.0;
  std::vector<double> predictive_horizons;
  std::vector<double> trj_time;
  std::vector<int> np;
  if (!nh.getParam("total_time",trj_time))
  {
    ROS_ERROR("total_time not found");
    return 0;
  }
  unsigned int number_instants=10;
  if (!nh.getParam("predictive_horizon",predictive_horizons))
  {
    ROS_ERROR("predictive_horizon not found");
    return 0;
  }
  if (!nh.getParam("number_instants",np))
  {
    ROS_ERROR("number_instants not found");
    return 0;
  }
  if (!nh.getParam("total_time",trj_time))
  {
    ROS_ERROR("total_time not found");
    return 0;
  }
  if (!nh.getParam("base_frame",base_frame))
  {
  }
  if (!nh.getParam("tool_frame",tool_frame))
  {
  }
  if (!nh.getParam("input_blocking",INPUT_BLK))
  {
  }

  double st=0.001;
 
  double lambda_acc=1e-20;
  double lambda_tau=0;
  double lambda_scaling=1e-2;//1e-2;
  double lambda_clik=2500;//1e0;
  
  urdf::Model model;
  model.initParam("robot_description");
  Eigen::Vector3d grav;
  grav << 0, 0, -9.806;
  boost::shared_ptr<rosdyn::Chain> chain = rosdyn::createChain(model,base_frame,tool_frame,grav);
  unsigned nax=chain->getActiveJointsNumber();
  Eigen::VectorXd qmax(6);
  Eigen::VectorXd qmin(6);
  Eigen::VectorXd Dqmax(6);
  Eigen::VectorXd DDqmax(6);
  Eigen::VectorXd tau_max(6);

  qmax.setConstant(10);
  qmin.setConstant(-10);
  Dqmax << 2, 2, 3, 3, 3, 3;
  DDqmax << 5, 5, 10, 10, 10, 10;
  tau_max << 200, 200, 100, 50, 50, 50;

  qmax.conservativeResize(nax);
  qmin.conservativeResize(nax);
  Dqmax.conservativeResize(nax);
  DDqmax.conservativeResize(nax);
  tau_max.conservativeResize(nax);

  ROS_INFO_NAMED(nh.getNamespace(),"CREATING THOR");
  thor::math::ThorQP thor;
  thor.setDynamicsChain(chain);
  
  for (unsigned int itime=0;itime<trj_time.size();itime++)
  {


    Ttot=trj_time.at(itime);
    for (unsigned int ipred=0;ipred<predictive_horizons.size();ipred++)
    {
      double predictive_horizon=predictive_horizons.at(ipred);
      number_instants=np.at(ipred);

      double control_horizon=predictive_horizon;
      ROS_INFO_NAMED(nh.getNamespace(),"SETTING THOR MATRICES");
      thor.setIntervals(number_instants,nax,control_horizon,st,INPUT_BLK);
      thor.setWeigthFunction(lambda_acc,lambda_tau,0.0,lambda_scaling,lambda_clik);
      thor.setConstraints(qmax,qmin,Dqmax,DDqmax,tau_max);
      thor.activatePositionBounds(true);
      thor.activateTorqueBounds(false);
      if (thor.needUpdate())
      {
        ROS_INFO_NAMED(nh.getNamespace(),"UPDATING THOR MATRICES");
        thor.updateMatrices();
      }
      Eigen::VectorXd prediction_time = thor.getPredictionTimeInstant();
      ROS_WARN_STREAM("pred_times="<<prediction_time.transpose());

      qstart.resize(6);
      qstart << 0.0, -2.0, 0.0, 0.0, 0.0, 0.5;
      qstart.conservativeResize(nax);
      amplitude.resize(nax);
      amplitude.setConstant(0.5);
      std::vector<double> temp;
      if (nh.getParam("qstart",temp))
      {
        for (unsigned int idx=0;idx<nax;idx++)
          qstart(idx)=temp.at(idx);
      }

      if (nh.getParam("amplitude",temp))
      {
        for (unsigned int idx=0;idx<nax;idx++)
          amplitude(idx)=temp.at(idx);
      }

      Eigen::VectorXd initial_state(2*nax);
      initial_state.setZero();
      initial_state.head(nax)=qstart;
      thor.setInitialState( initial_state );
      Eigen::VectorXd target_Dq(number_instants*nax);
      target_Dq.setZero();
      double target_scaling=1;
      Eigen::VectorXd next_Q(nax);
      next_Q=initial_state.head(nax);
      Eigen::VectorXd next_acc(nax);
      double scaling=1;
      ros::Time init=ros::Time::now();

      ROS_INFO("solving trajectory");
      double current_time=0.0;
      double nominal_t=0;
      double max_computing_time=0.0;
      double mean_scaling=1.0;
      double mean_computing_time=0.0;

      std::string test_name = "nmpc_qp_Ttot_" + std::to_string(int(Ttot)) + "_Np" + std::to_string(int(number_instants)) + "_T" + std::to_string(int(predictive_horizon*1000));
      moveit_msgs::ExecuteTrajectoryGoal goal;
      goal.trajectory.joint_trajectory.joint_names.resize(nax);
      goal.trajectory.joint_trajectory.joint_names=chain->getMoveableJointNames();

      ros::Duration time=ros::Duration(0.0);
      std::string status="success";
      for (int idx=0;idx<int((Ttot+1)/st);idx++)
      {
        current_time+=st;
        nominal_t+=scaling*st;

        Eigen::VectorXd tmp_pos(nax);
        Eigen::VectorXd tmp_vel(nax);

        trj(nominal_t+prediction_time(0),nax,tmp_pos,tmp_vel);
        next_Q=tmp_pos;
        target_Dq.segment(0,nax)=tmp_vel;
        for (unsigned int ic=1;ic<number_instants;ic++)
        {
          trj(nominal_t+prediction_time(ic),nax,tmp_pos,tmp_vel);
          target_Dq.segment(ic*nax,nax)=tmp_vel;
        }
        ros::Time t0=ros::Time::now();
    //     thor.computedUncostrainedSolution(target_Dq,next_Q,target_scaling,thor.getState(),next_acc,scaling);
        thor.computedCostrainedSolution(target_Dq,next_Q,target_scaling,thor.getState(),next_acc,scaling);
        double tcalc=(ros::Time::now()-t0).toSec()*1e3;
        thor.updateState(next_acc);

        Eigen::VectorXd q_ref   =thor.getState().head(nax);
        Eigen::VectorXd dq_ref  =thor.getState().tail(nax);
        Eigen::VectorXd ddq_ref  =next_acc;
        Eigen::VectorXd tau_ref =chain->getJointTorque(q_ref,dq_ref,ddq_ref);

        trajectory_msgs::JointTrajectoryPoint pnt;
        pnt.positions.resize(nax);
        pnt.velocities.resize(nax);
        pnt.effort.resize(nax);
        /* log */
        for (unsigned int idof=0;idof<nax;idof++)
        {
          pnt.positions.at(idof)=q_ref(idof);
          pnt.velocities.at(idof)=dq_ref(idof);
          pnt.effort.at(idof)=tau_ref(idof);
        }
        pnt.time_from_start=time;
        time+=ros::Duration(st);
        goal.trajectory.joint_trajectory.points.push_back(pnt);

        if (tcalc>=max_computing_time)
        {
          max_computing_time=tcalc;
          ROS_ERROR_STREAM("max t calc [ms] =" << max_computing_time << ", scaling = " << scaling);
        }
        mean_computing_time=((mean_computing_time)*idx+tcalc)/double(idx+1);
        mean_scaling=(mean_scaling*idx+scaling)/double(idx+1);
        ROS_INFO("test=%s, perc=%f%%",test_name.c_str(),current_time/(Ttot+1)*100);

      }
      ROS_ERROR("mean scaling = %f",mean_scaling);
      ROS_ERROR("max computing time = %f [ms]",max_computing_time);
      ROS_ERROR("mean computing time = %f [ms]",mean_computing_time);

      trajectory_processing::setTrajectoryToParam(nh,"nmpc",goal.trajectory.joint_trajectory);

      nh.setParam("/acado_results/"+test_name+"/status",status);

      nh.setParam("acado_results/"+test_name+"/mean_scaling",mean_scaling);
      nh.setParam("acado_results/"+test_name+"/max_computing_time",max_computing_time);
      nh.setParam("acado_results/"+test_name+"/mean_computing_time",mean_computing_time);

      trajectory_processing::setTrajectoryToParam(nh,"acado_results/"+test_name+"/trj",goal.trajectory.joint_trajectory);

      system("rosparam dump nmpc_qp.yaml acado_results/");
    }
  }

  return 0;  
}
