/*
 *    This file is part of ACADO Toolkit.
 *
 *    ACADO Toolkit -- A Toolkit for Automatic Control and Dynamic Optimization.
 *    Copyright (C) 2008-2014 by Boris Houska, Hans Joachim Ferreau,
 *    Milan Vukov, Rien Quirynen, KU Leuven.
 *    Developed within the Optimization in Engineering Center (OPTEC)
 *    under supervision of Moritz Diehl. All rights reserved.
 *
 *    ACADO Toolkit is free software; you can redistribute it and/or
 *    modify it under the terms of the GNU Lesser General Public
 *    License as published by the Free Software Foundation; either
 *    version 3 of the License, or (at your option) any later version.
 *
 *    ACADO Toolkit is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *    Lesser General Public License for more details.
 *
 *    You should have received a copy of the GNU Lesser General Public
 *    License along with ACADO Toolkit; if not, write to the Free Software
 *    Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 *
 */

#include <acado_code_generation.hpp>

USING_NAMESPACE_ACADO


void funz( double *x, double *f, void *user_data )
{

//  int m_ndof=1;

//  Eigen::Map<Eigen::VectorXd> ix(x,m_ndof);
//  Eigen::Map<Eigen::VectorXd> dx(f,m_ndof);
//  dx<<-ix;
  *f = *x;
}

void der_funz( int number, double* x, double* seed, double* f, double* df, void *userData )
{

//  int m_ndof=1;

//  Eigen::Map<Eigen::VectorXd> ix(x,m_ndof);
//  Eigen::Map<Eigen::VectorXd> dx(f,m_ndof);
//  dx<<-ix;
  *df = 1;
}

int main(int argc, char * const argv[ ])
{
  //
  // Variables
  //

  DifferentialState	x;
  Control				F;

  ACADO::CFunction funzione( 1, funz,der_funz,der_funz);


  double aa=5;
  double bb=0;
  double cc=0;
  funzione.evaluate(1,&aa,&bb);
  printf("a=%f,b=%f\n",aa,bb);
  funzione.AD_backward(1,&aa,&cc);
  printf("a=%f,c=%f\n",aa,cc);
  funzione.AD_forward(1,&aa,&cc);
  printf("a=%f,c=%f\n",aa,cc);

  //
  // Differential algebraic equation
  //

  DifferentialEquation f;

  f << funzione(F);
//  f << F;

  //
  // Weighting matrices and reference functions
  //

  Function rf;
  Function rfN;

  rf << x;
  rfN << x;

  DMatrix W = eye<double>( rf.getDim() );
  DMatrix WN = eye<double>( rfN.getDim() ) * 1;

  //
  // Optimal Control Problem
  //

  const int N  = 10;
  const int Ni = 1;
  const double Ts = 0.1;

  OCP ocp(0, N * Ts, N);

  ocp.subjectTo( f );

  ocp.minimizeLSQ(W, rf);
  ocp.minimizeLSQEndTerm(WN, rfN);

  ocp.subjectTo(-20 <= F <= 20);
  //
  // Export the code:
  //
  OCPexport mpc( ocp );

  mpc.set(HESSIAN_APPROXIMATION, GAUSS_NEWTON);
  mpc.set(DISCRETIZATION_TYPE, SINGLE_SHOOTING);

  mpc.set(INTEGRATOR_TYPE, INT_DISCRETE);
  mpc.set(NUM_INTEGRATOR_STEPS, N * Ni);

  if (mpc.exportCode( "test_autogen" ) != SUCCESSFUL_RETURN)
    exit( EXIT_FAILURE );

  mpc.printDimensionsQP( );

  return EXIT_SUCCESS;
}
