cmake_minimum_required(VERSION 2.8.3)
project(thor_acado)

add_compile_options(-std=c++11 -funroll-loops -Wall -Ofast)
set(CMAKE_BUILD_TYPE Release)

find_package(catkin REQUIRED COMPONENTS
  roscpp
  thor_math
  actionlib
  moveit_msgs
  moveit_planning_helper
)
find_package( ACADO REQUIRED )

MESSAGE("ACADO_INCLUDE_DIRS")
MESSAGE(   ${ACADO_INCLUDE_DIRS})
MESSAGE("ACADO_SHARED_LIBRARIES")
MESSAGE(   ${ACADO_SHARED_LIBRARIES})



catkin_package(
  LIBRARIES thor_acado
  CATKIN_DEPENDS roscpp thor_math moveit_msgs actionlib moveit_planning_helper
  DEPENDS ACADO
)

###########
## Build ##
###########

include_directories(
  ${catkin_INCLUDE_DIRS}
  ${ACADO_INCLUDE_DIRS}
)

add_executable(example_NMPC_torque src/example_NMPC_torque.cpp)
add_dependencies(example_NMPC_torque ${${PROJECT_NAME}_EXPORTED_TARGETS} ${catkin_EXPORTED_TARGETS} ${ACADO_EXPORTED_TARGETS})
target_link_libraries(example_NMPC_torque
  ${catkin_LIBRARIES}
  ${ACADO_SHARED_LIBRARIES}
  ${ACADO_LIBRARIES}
)

add_executable(example_NMPC_acc src/example_NMPC_acc.cpp)
add_dependencies(example_NMPC_acc ${${PROJECT_NAME}_EXPORTED_TARGETS} ${catkin_EXPORTED_TARGETS} ${ACADO_EXPORTED_TARGETS})
target_link_libraries(example_NMPC_acc
  ${catkin_LIBRARIES}
  ${ACADO_SHARED_LIBRARIES}
  ${ACADO_LIBRARIES}
)
add_executable(example_NMPC_acc_ib src/example_NMPC_acc_ib.cpp)
add_dependencies(example_NMPC_acc_ib ${${PROJECT_NAME}_EXPORTED_TARGETS} ${catkin_EXPORTED_TARGETS} ${ACADO_EXPORTED_TARGETS})
target_link_libraries(example_NMPC_acc_ib
  ${catkin_LIBRARIES}
  ${ACADO_SHARED_LIBRARIES}
  ${ACADO_LIBRARIES}
)
add_executable(example_QP_approx src/example_QP_approx.cpp)
add_dependencies(example_QP_approx ${${PROJECT_NAME}_EXPORTED_TARGETS} ${catkin_EXPORTED_TARGETS} ${ACADO_EXPORTED_TARGETS})
target_link_libraries(example_QP_approx
  ${catkin_LIBRARIES}
  ${ACADO_SHARED_LIBRARIES}
  ${ACADO_LIBRARIES}
)
